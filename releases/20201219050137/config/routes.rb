Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "users#index"
  get 'log_in' => 'sessions#new', :as => :log_in
  post 'log_out' => 'sessions#destroy', :as => :log_out
  # get '/log_in' => 'sessions#new', :as => :log_in
  # post '/log_out' => 'sessions#destroy', :as => :log_out

  resources :users
  get '/sign_up', to: 'users#new', as: :sign_up
  resources :sessions, only: [:new, :create, :destroy]
end
