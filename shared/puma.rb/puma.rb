#!/usr/bin/env puma

directory '/home/deploy/ecran-village-plannings/current'
rackup "/home/deploy/ecran-village-plannings/current/config.ru"
environment 'production'

tag ''

pidfile "/home/deploy/ecran-village-plannings/shared/tmp/pids/puma.pid"
state_path "/home/deploy/ecran-village-plannings/shared/tmp/pids/puma.state"
stdout_redirect '/home/deploy/ecran-village-plannings/current/log/puma.error.log', '/home/deploy/ecran-village-plannings/current/log/puma.access.log', true


threads 4,16



bind 'unix:///home/deploy/ecran-village-plannings/shared/tmp/sockets/ecran-village-plannings-puma.sock'

workers 2




restart_command 'bundle exec puma'


preload_app!


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = ""
end


before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end

