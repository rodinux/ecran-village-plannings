# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

server 'seances.ecranvillage.ovh', roles: [:web, :app, :db], primary: true

set :repo_url, "git@framagit.org:rodinux/ecran-village-plannings.git"
set :application, "EcranVillagePlannings"
# set :scm, :git
ask :branch, 'master'
set :bundle_without, %w{development test}.join(' ')

set user: 'deploy', roles: %w{app db web}

set :log_level, :debug
# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true
set :deploy_to,       "/home/deploy/#{fetch(:application)}"
# Default value for :linked_files is []
append :linked_files, "config/master.key", "config/database.yml"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle" "public/system", "storage"

# Default value for default_env is {}
#set :default_env, { path: "/opt/ruby/bin:$PATH" }
#set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
#set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads' )

# Rbenv specific settings
set :rbenv_path, '/home/deploy/.rbenv'
#set :rbenv_type, :system # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.7.2'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

set :rails_env, 'production'
# Default value for keep_releases is 5
set :keep_releases, 5
# if you want to remove the dump file after loading
# set :db_local_clean, true
# if you want to remove the dump file from the server after downloading
# set :db_remote_clean, true
# if you are highly paranoid and want to prevent any push operation to the server
# set :disallow_pushing, false

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
set :puma_threads,    [4, 16]
set :puma_workers,    2
#set :use_sudo,        false
set :stage,           :production
# set :deploy_via,      :remote_cache
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { foward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa) }
set :puma_preload_app, true
set :rails_env, 'priduction'
set :puma_env, 'production'

set :nginx_config_name, 'ecran_village_plannings'
set :nginx_server_name, 'seances.ecranvillage.ovh'

set :puma_preload_app, true
# set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :puma_workers, 2

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  #after  :finishing,    :restart
end
# when using db, you should add config/database.yml here
# set :linked_files, fetch(:linked_files, []).concat(%w{.rbenv-vars})
# set :linked_dirs, fetch(:linked_dirs, []).concat(%w{log tmp/pids tmp/cache tmp/sockets vendor/bundle})
SSHKit.config.command_map.prefix[:puma].push("bundle exec")
SSHKit.config.command_map.prefix[:pumactl].push("bundle exec")
